/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *	 this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *	 this list of conditions and the following disclaimer in the documentation
 *	 and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *	 may be used to endorse or promote products derived from this software
 *	 without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *	 ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <termios.h>
#include <HAP_farf.h>
#include <sys/ioctl.h>
#include <dspal_time.h>

#include <rangefinder_rpc.h> // rpc functions from our IDL
#include "vl53l1x.h" // shared header

#include <common.h>
#include <vl53l1x_registers.h>

#include "dev_fs_lib_i2c.h"

#define PRINT(...)  FARF(ALWAYS, __VA_ARGS__);

// file descriptors for all possible i2c ports
static int i2c_fds[MAX_NUM_I2C_PORTS];

//ports that DSPaL does not allow opening: 0 and > 12
//ports that DSPaL allows opening and reading, but should be blocked : 1, 10 (using by on-board SPI IMUs)
//ports that DSPaL allows opening, but causes board crash : 2,4,5,11,12
//ports that work : 3,6,7,8
//ports that DSPaL allows opening and does not crash, need to be actually tested : 9 (marked as GPIO) on J7
static uint32_t i2c_port_whitelist[] = {3,6,7,8,9};


// local copy of rangefinder sensor configuration data
rangefinder_rpc_sensor_data_for_sdsp_t sensor_data;
int is_sensor_data_set = 0;


int64_t get_sdsp_time_realtime_ns()
{
	struct timespec ts;
	if(clock_gettime(CLOCK_REALTIME, &ts)) return -1;
	int64_t t = (int64_t)ts.tv_sec*1000000000 + (int64_t)ts.tv_nsec;
	return t;
}

static int check_i2c_bus_num(uint32_t i2c_bus, const char * calling_func)
{
	if(i2c_bus>=MAX_NUM_I2C_PORTS){
		PRINT("ERROR in %s: i2c bus number must be between 0 & %d (provided %d)", calling_func, MAX_NUM_I2C_PORTS-1,i2c_bus);
		return -1;
	}

	int valid_bus_num = 0;  //true / false
	int whitelist_size = sizeof(i2c_port_whitelist) / sizeof(i2c_port_whitelist[0]);
	int ii;
	for (ii=0;ii<whitelist_size;ii++){
		if (i2c_bus == i2c_port_whitelist[ii]){
			valid_bus_num = 1;
			break;
		}
	}

	if (valid_bus_num == 0){
		PRINT("ERROR in %s: i2c bus number %d is invalid", calling_func, i2c_bus);
		return -1;
	}

	return 0;
}

static int check_i2c_bus_init_status(uint32_t i2c_bus, const char * calling_func)
{
	if(i2c_fds[i2c_bus]==0){
		PRINT("ERROR in %s: i2c bus %d is not initialized", calling_func, i2c_bus);
		return -1;
	}
	return 0;
}

int rangefinder_rpc_i2c_init(uint32_t i2c_bus)
{
	if (check_i2c_bus_num(i2c_bus,__func__))
		return -1;

	if(i2c_fds[i2c_bus] != 0){
		PRINT("WARNING in rangefinder_rpc_i2c_init, trying to initialize i2c bus %d when already initialized", i2c_bus);
		return 0;
	}

	char device_path[16];
	snprintf(device_path,sizeof(device_path),"/dev/iic-%d", (int)i2c_bus);
	int i2c_fd = open(device_path,0);

	if (i2c_fd == -1){
		PRINT("ERROR in rangefinder_rpc_i2c_init: could not open device %s",device_path);
		return -1;
	}

	i2c_fds[i2c_bus] = i2c_fd;

	return 0;
}

int rangefinder_rpc_i2c_slave_config(uint32_t i2c_bus, uint32_t slave_address, uint32_t bit_rate, uint32_t timeout_us)
{
	if (check_i2c_bus_num(i2c_bus,__func__))
		return -1;

	if(check_i2c_bus_init_status(i2c_bus,__func__))
		return -1;

	struct dspal_i2c_ioctl_slave_config slave_config;
	memset(&slave_config, 0, sizeof(slave_config));

	slave_config.slave_address					  = slave_address & 1023;  //TODO: do we support 10-bit slave addressing??
	slave_config.bus_frequency_in_khz			 = bit_rate/1000;
	slave_config.byte_transer_timeout_in_usecs = timeout_us;
	if (ioctl(i2c_fds[i2c_bus], I2C_IOCTL_SLAVE, &slave_config) != 0){
		PRINT("ERROR in rangefinder_rpc_i2c_slave_config, i2c bus %d, slave address %d, bit rate %d, timeout %d",i2c_bus, slave_address, bit_rate, timeout_us);
		return -1;
	}

	return 0;
}

int rangefinder_rpc_i2c_read(uint32_t i2c_bus, uint32_t register_address, uint32_t register_address_num_bits, uint8_t * read_buffer, int read_length)
{
	//int64_t t_i2c_read_start = get_sdsp_time_realtime_ns();
	if (check_i2c_bus_num(i2c_bus,__func__))
		return -1;

	if(check_i2c_bus_init_status(i2c_bus,__func__))
		return -1;

	int reg_addr_len_bytes = 1;
	switch (register_address_num_bits){
		case 8: case 16: case 24: case 32:
			reg_addr_len_bytes = register_address_num_bits / 8;
			break;
		default:
			PRINT("ERROR in rangefinder_rpc_i2c_read: provided invalid number of bits in register address: %d (acceptable values are 8, 16, 24, 32)",register_address_num_bits);
			return -1;
	}

	// we will first write the register address to read from then read the data (in one transaction)
	struct dspal_i2c_ioctl_combined_write_read ioctl_write_read;

	ioctl_write_read.write_buf	  = (uint8_t*)(&register_address);
	ioctl_write_read.write_buf_len = reg_addr_len_bytes;
	ioctl_write_read.read_buf		= read_buffer;
	ioctl_write_read.read_buf_len  = read_length;

	int bytes_read = ioctl(i2c_fds[i2c_bus], I2C_IOCTL_RDWR, &ioctl_write_read);
	if (bytes_read != read_length)
	{
		PRINT("ERROR in rangefinder_rpc_i2c_read: read %d bytes, but requested %d bytes",bytes_read,read_length);
		return -1;
	}

	//int64_t t_i2c_read_end = get_sdsp_time_realtime_ns();
	//PRINT("I2C read time = %dus",(int32_t)(t_i2c_read_end-t_i2c_read_start)/1000);

	return 0;
}

int rangefinder_rpc_i2c_write(uint32_t i2c_bus, uint32_t register_address, uint32_t register_address_num_bits, const uint8_t * write_buffer, int write_length)
{
	//int64_t t_i2c_write_start = get_sdsp_time_realtime_ns();

	if (check_i2c_bus_num(i2c_bus,__func__))
		return -1;

	if(check_i2c_bus_init_status(i2c_bus,__func__))
		return -1;

	int reg_addr_len_bytes = 1;
	switch (register_address_num_bits){
		case 8: case 16: case 24: case 32:
			reg_addr_len_bytes = register_address_num_bits / 8;
			break;
		default:
			PRINT("ERROR in rangefinder_rpc_i2c_write: provided invalid number of bits in register address: %d (acceptable values are 8, 16, 24, 32)",register_address_num_bits);
			return -1;
	}

	int write_length_full = write_length + reg_addr_len_bytes;

	if (write_length_full > MAX_I2C_TX_BUFFER_SIZE)
	{
		PRINT("ERROR in rangefinder_rpc_i2c_write : Caller's buffer size (%d) exceeds size of local buffer (%d)",write_length_full,MAX_I2C_TX_BUFFER_SIZE);
		return -1;
	}

	uint8_t write_buffer_full[MAX_I2C_TX_BUFFER_SIZE];

	memcpy(write_buffer_full,&register_address,reg_addr_len_bytes);
	memcpy(&write_buffer_full[reg_addr_len_bytes], write_buffer, write_length);
	int bytes_written = write(i2c_fds[i2c_bus], (char *)write_buffer_full, write_length_full);
	if (bytes_written != write_length_full)
	{
		PRINT("ERROR in rangefinder_rpc_i2c_write: wrote %d bytes, but requested %d bytes (including target start register)",bytes_written,write_length_full);
		return -1;
	}

	//int64_t t_i2c_write_end = get_sdsp_time_realtime_ns();
	//PRINT("I2C write time = %dus",(int32_t)(t_i2c_write_end-t_i2c_write_start)/1000);

	return 0;
}

int rangefinder_rpc_i2c_close(uint32_t i2c_bus)
{
	if (check_i2c_bus_num(i2c_bus,__func__))
		return -1;

	// if not initialized already, return
	if(i2c_fds[i2c_bus]==0) return 0;

	close(i2c_fds[i2c_bus]);
	i2c_fds[i2c_bus]=0;
	return 0;
}










// reverse lsb and msb bytes of a 16-bit register for DSPAL
static uint32_t _reverse_lsb_msb_16(uint16_t reg)
{
	uint32_t out = reg >> 8;
	out |= (reg & 0xff) << 8;
	return out;
}



static int _set_slave(int mux_ch, uint8_t addr)
{
	// first set slave address to the multiplexer
	const int i2c_bit_rate_hz	= 400000;
	const int i2c_timeout_us	= 1000;

	if(rangefinder_rpc_i2c_slave_config(sensor_data.bus, sensor_data.mux_address, i2c_bit_rate_hz, i2c_timeout_us)){
		PRINT("failed to set i2c slave config on bus %d, address %d, bit rate %d, timeout %dus\n",
				sensor_data.bus, sensor_data.mux_address,i2c_bit_rate_hz,i2c_timeout_us);
		return -1;
	}

	// set bitmask for mux channels to open
	uint8_t bitmask;
	if(mux_ch>7){	// all
		bitmask = 0xFF;
	}
	else if(mux_ch<0){	// none
		bitmask = 0;
	}
	else{	// just one
		bitmask = 1 << mux_ch;
	}

	if(rangefinder_rpc_i2c_write(sensor_data.bus, bitmask, 8, &bitmask, 0)){
		PRINT("failed to write to i2c multiplexer\n");
		return -1;
	}

	// then put address back to the rangefinder
	// vl53l1x wants the address bitshifted right for some unknown reason
	if(rangefinder_rpc_i2c_slave_config(sensor_data.bus, addr, i2c_bit_rate_hz, i2c_timeout_us)){
		PRINT("failed to set i2c slave config on bus %d, address %d, bit rate %d, timeout %dus\n",
				sensor_data.bus, addr, i2c_bit_rate_hz,i2c_timeout_us);
		return -1;
	}
	return 0;

}



static int vl53l1x_write_reg_byte(uint16_t reg, uint8_t data)
{
	return rangefinder_rpc_i2c_write(sensor_data.bus, _reverse_lsb_msb_16(reg), 16, &data, 1);
}

static int vl53l1x_write_reg_word(uint16_t reg, uint16_t data)
{
	uint8_t buf[2];
	buf[0] = data >> 8;
	buf[1] = data & 0x00FF;
	return rangefinder_rpc_i2c_write(sensor_data.bus, _reverse_lsb_msb_16(reg), 16, buf, 2);
}

static int vl53l1x_write_reg_int(uint16_t reg, uint32_t data)
{
	uint8_t buf[4];
	buf[0] = (data >> 24) & 0xFF;
	buf[1] = (data >> 16) & 0xFF;
	buf[2] = (data >> 8)  & 0xFF;
	buf[3] = (data >> 0)  & 0xFF;
	return rangefinder_rpc_i2c_write(sensor_data.bus, _reverse_lsb_msb_16(reg), 16, buf, 4);
}

static int vl53l1x_read_reg_byte(uint16_t reg, uint8_t* data)
{
	int ret;
	ret = rangefinder_rpc_i2c_read(sensor_data.bus, _reverse_lsb_msb_16(reg), 16, data, 1);
	return ret;
}


static int vl53l1x_read_reg_word(uint16_t reg, uint16_t* data)
{
	int ret;
	uint8_t buf[2];
	ret = rangefinder_rpc_i2c_read(sensor_data.bus, _reverse_lsb_msb_16(reg), 16, buf, 2);
	if(ret) return -1;
	*data = (buf[0] << 8) + buf[1];
	return 0;
}


static int vl53l1x_start_ranging()
{
	return vl53l1x_write_reg_byte(SYSTEM__MODE_START, 0x40); /* Enable VL53L1X */
}

static int vl53l1x_stop_ranging()
{
	return vl53l1x_write_reg_byte(SYSTEM__MODE_START, 0x00); /* Disable VL53L1X */
}

static int vl53l1x_clear_interrupt()
{
	return vl53l1x_write_reg_byte(SYSTEM__INTERRUPT_CLEAR, 0x01);
}

static int vl53l1x_check_for_data_ready(uint8_t *isDataReady)
{
	uint8_t Temp;

	if(vl53l1x_read_reg_byte(GPIO__TIO_HV_STATUS, &Temp)){
		return -1;
	}

	if(Temp & 1){
		*isDataReady = 1;
	}
	else{
		*isDataReady = 0;
	}
	return 0;
}

static int vl53l1x_get_distance_mm(int* dist_mm)
{
	// first check status, if it's not valid, don't bother reading the distance
	uint8_t status;
	if(vl53l1x_read_reg_byte(VL53L1_RESULT__RANGE_STATUS, &status)){
		return -1;
	}
	status = status & 0x1F;
	if(status!=9){
		*dist_mm = -1000;
		return 0;
	}

	uint16_t tmp;
	if(vl53l1x_read_reg_word(VL53L1_RESULT__FINAL_CROSSTALK_CORRECTED_RANGE_MM_SD0, &tmp)){
		return -1;
	}
	*dist_mm = tmp;
	return 0;
}

static int vl53l1x_wait_for_data()
{
	for(int i=0; i<100; i++){
		uint8_t isDataReady = 0;
		if(vl53l1x_check_for_data_ready(&isDataReady)){
			PRINT("failed to check data ready\n");
			return -1;
		}
		if(isDataReady){
			return 0;
		}
		//printf("data ready: %d i=%d\n", isDataReady, i);
		usleep(1000);
	}

	// timeout
	return -1;
}











//#define DEBUG

// make local copy of the sensor data
int rangefinder_rpc_set_sensor_data(const rangefinder_rpc_sensor_data_for_sdsp_t* data)
{
	sensor_data = *data;
	is_sensor_data_set = 1;
	return 0;
}

int rangefinder_rpc_read_all_rangefinders(uint8_t* result, int result_length)
{
	int i;

	if(is_sensor_data_set==0){
		PRINT("ERROR, trying to call rangefinder_rpc_read_all_rangefinders before setting sensor data");
		return -1;
	}


	// small array to keep the distances in
	int dist_mm[32];

	// start the standalone sensor ranging if it exists
	if(sensor_data.has_nonmux_sensor){
		_set_slave(MUX_NONE, sensor_data.nonmux_sensor_address);
		if(vl53l1x_start_ranging()){
			PRINT("failed to start ranging");
			return -1;
		}
	}
	// start all the multiplexed sensors reading at the same time
	if(sensor_data.n_mux_sensors>0){
		_set_slave(MUX_ALL, VL53L1X_TOF_DEFAULT_ADDRESS);
		if(vl53l1x_start_ranging()){
			PRINT("failed to start ranging");
			return -1;
		}
	}

	// sleep a bit while they range, this should be less than the actual
	// ranging time so we can poll them at the end of the ranging process
	if(sensor_data.wait_time_us > 0){
		usleep(sensor_data.wait_time_us);
	}

	#ifdef DEBUG
	PRINT("starting to read all sensors");
	#endif

	// now start reading the data back in
	for(i=0;i<sensor_data.n_enabled_sensors;i++){

		#ifdef DEBUG
		PRINT("in loop");
		#endif

		// switch i2c bus and multiplexer over to either a multiplexed or non-multiplexed sensor
		if(i != sensor_data.nonmux_sensor_index){
			#ifdef DEBUG
			PRINT("setting mux to one port");
			#endif
			_set_slave(sensor_data.mux_port[i], VL53L1X_TOF_DEFAULT_ADDRESS);
		}
		else{
			#ifdef DEBUG
			PRINT("setting multiplexer to none");
			#endif
			_set_slave(MUX_NONE, sensor_data.nonmux_sensor_address);
		}

		// wait for it to be done ranging
		#ifdef DEBUG
		PRINT("waiting for data");
		#endif
		if(vl53l1x_wait_for_data()){
			return -1;
		}

		// read in the data and stop it ranging
		#ifdef DEBUG
		PRINT("reading distance");
		#endif
		vl53l1x_get_distance_mm(&dist_mm[i]);
		vl53l1x_clear_interrupt();
		vl53l1x_stop_ranging();
	}

	// copy the data into the shared memory for return
	memcpy(result, dist_mm, sensor_data.n_enabled_sensors*sizeof(int));

	return 0;
}
