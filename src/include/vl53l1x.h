/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/



#ifndef VOXL_vl53l1x_H
#define VOXL_vl53l1x_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define I2C_J11	12	// BLSP 12  on physical port J11 pins 4 (sda) and 6 (SCL)
#define I2C_J10	7	// BLSP 7   on physical port J10 pins 4 (sda) and 6 (SCL)

/*******************************************************************************
 * RPC Shared Memory
 ******************************************************************************/

/**
 * @brief      allocates memory shared between SDSP and APPS proc
 *
 * @param[in]  bytes  bytes to allocate
 *
 * @return     pointer to memory on success, NULL on failure
 */
uint8_t* voxl_rpc_shared_mem_alloc(size_t bytes);


/**
 * @brief      frees previously allocated RPC shared memory
 *
 * @param      ptr   pointer to memory
 */
void voxl_rpc_shared_mem_free(uint8_t* ptr);


/**
 * @brief      deinitiazes the RPC shared memory system
 */
void voxl_rpc_shared_mem_deinit();


/*******************************************************************************
 * I2C
 ******************************************************************************/

#define MAX_NUM_I2C_PORTS 16 //highest number of i2c port (BLSP #)
#define MAX_I2C_TX_BUFFER_SIZE 128

/**
 * @brief      Initializes an I2C port
 *
 *             Note that I2C bus speed and slave ID are sent using voxl_i2c_slave_config
 *
 * @param[in]  bus       I2C bus number (BLSP #)
 *
 * @return     0 on success, -1 on failure
 */
int voxl_i2c_init(uint32_t bus);

/**
 * @brief      Closes an I2C port
 *
 * @param[in]  bus       I2C bus number (BLSP #)
 *
 * @return     0 on success, -1 on failure
 */
int voxl_i2c_close(uint32_t bus);

/**
 * @brief      Configures I2C port for communication with a certain i2c slave ID
 *
 * @param[in]  bus                  I2C bus number (BLSP #)
 * @param[in]  slave_address_7bit   I2C slave address (7bit format, 7bit only (10 bit slave addresses are not supported))
 * @param[in]  bit_rate             I2C bus bit rate (e.g 100000, 400000)
 * @param[in]  timeout_us           Timeout for transfer (in microseconds)
 *
 * @return     0 on success, -1 on failure
 */
int voxl_i2c_slave_config(uint32_t bus, uint16_t slave_address, uint32_t bit_rate, uint32_t timeout_us);

/**
 * @brief      Write data to I2C slave device starting from the specified 8-bit register address
 *
 * @param[in]  bus                  I2C bus number (BLSP #)
 * @param[in]  register_address     Register address to access on the device (note: byte order is LS Byte first if > 8 bits)
 * @param[in]  address_num_bits     Number of bits to use for sending register address (8, 16, 24, or 32)
 * @param[in]  output_buffer        Buffer with data to send to the device
 * @param[in]  length               Number of bytes to send to device (in addition to the register address)
 *
 * @return     0 on success, -1 on failure
 */
int voxl_i2c_write(uint32_t bus, uint32_t register_address, uint32_t address_num_bits, uint8_t * write_buffer, uint32_t write_length);

/**
 * @brief      Read data from I2C slave device starting from the specified 8-bit register address
 *
 * @param[in]  bus                  I2C bus number (BLSP #)
 * @param[in]  register_address     Register address to access on the device (note: byte order is LS Byte first if > 8 bits)
 * @param[in]  address_num_bits     Number of bits to use for sending register address (8, 16, 24, or 32)
 * @param[in]  input_buffer         Buffer to hold the incoming data
 * @param[in]  length               Number of bytes to read from device
 *
 * @return     0 on success, -1 on failure
 */
int voxl_i2c_read(uint32_t bus, uint32_t register_address, uint32_t address_num_bits, uint8_t * read_buffer, uint32_t read_length);


#ifdef __cplusplus
}
#endif

#endif // VOXL_vl53l1x_H
