/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR busINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#include <stdio.h>
#include <stdlib.h> // for exit()
#include <signal.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>

#include <vl53l1x.h>

#include <modal_start_stop.h>
#include <modal_pipe_server.h>
#include <common.h>
#include <rangefinder_rpc.h>		// sdsp rpc calls from idl file
#include "voxl_rangefinder_interface.h"
#include "config_file.h"
#include "rpcmem.h"

#include <vl53l1x_registers.h>
#include <sx1509_registers.h>

#define SERVER_NAME	"voxl-rangefinder-server"
#define PIPE_CH		0

#define SDSP_ACCELERATION // enable the sdsp offload function

static int running = 0;
static int en_debug = 0;
static int en_config_mode = 0;
static int config_arrangement = 0;
static int* dist_mm = NULL; // rpc shared memory




// printed if some invalid argument was given
static void print_usage(void)
{
	printf("\n\
voxl-rangefinder-server usually runs as a systemd background service. However, for debug\n\
purposes it can be started from the command line manually with any of the following\n\
debug options. When started from the command line, modal-hello-server will automatically\n\
stop the background service so you don't have to stop it manually\n\
\n\
use the --config argument to reset the config file back to one of the following\n\
default sensor arrangements\n\
\n\
4_TOF_ON_BREADBOARD_MUX    1\n\
8_TOF_ON_BREADBOARD_MUX    2\n\
10_TOF_ON_VOXLCAM_SEEKER   3\n\
\n\
-c, --config {config #}     set config file to default configuration\n\
-d, --debug                 print debug info\n\
-h, --help                  print this help message\n\
\n");
	return;
}


static int __parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"config",				required_argument,	0,	'c'},
		{"debug",				no_argument,		0,	'd'},
		{"help",				no_argument,		0,	'h'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "c:dh", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'c':
			en_config_mode = 1;
			config_arrangement = atoi(optarg);

		case 'd':
			en_debug = 1;
			break;

		case 'h':
			print_usage();
			return -1;

		default:
			print_usage();
			return -1;
		}
	}

	return 0;
}


int64_t _apps_time_monotonic_ns(void)
{
	struct timespec ts;
	if(clock_gettime(CLOCK_MONOTONIC, &ts)){
		fprintf(stderr,"ERROR calling clock_gettime\n");
		return -1;
	}
	return (int64_t)ts.tv_sec*1000000000 + (int64_t)ts.tv_nsec;
}


// reverse lsb and msb bytes of a 16-bit register for DSPAL
uint32_t _reverse_lsb_msb_16(uint16_t reg)
{
	uint32_t out = reg >> 8;
	out |= (reg & 0xff) << 8;
	return out;
}



int _set_slave(int mux_ch, uint8_t addr)
{
	// first set slave address to the multiplexer
	const int i2c_bit_rate_hz	= 400000;
	const int i2c_timeout_us	= 1000;

	if (voxl_i2c_slave_config(bus, mux_address, i2c_bit_rate_hz, i2c_timeout_us)){
		fprintf(stderr, "failed to set i2c slave config on bus %d, address %d, bit rate %d, timeout %dus\n",
				bus, mux_address,i2c_bit_rate_hz,i2c_timeout_us);
		return -1;
	}

	// set bitmask for mux channels to open
	uint8_t bitmask;
	if(mux_ch>7){	// all
		bitmask = 0xFF;
	}
	else if(mux_ch<0){	// none
		bitmask = 0;
	}
	else{	// just one
		bitmask = 1 << mux_ch;
	}

	if(voxl_i2c_write(bus, bitmask, 8, &bitmask, 0)){
		fprintf(stderr, "failed to write to i2c multiplexer\n");
		return -1;
	}

	// then put address back to the rangefinder
	// vl53l1x wants the address bitshifted right for some unknown reason
	if (voxl_i2c_slave_config(bus, addr, i2c_bit_rate_hz, i2c_timeout_us)){
		fprintf(stderr, "failed to set i2c slave config on bus %d, address %d, bit rate %d, timeout %dus\n",
				bus, addr, i2c_bit_rate_hz,i2c_timeout_us);
		return -1;
	}
	return 0;

}



int vl53l1x_write_reg_byte(uint16_t reg, uint8_t data)
{
	return voxl_i2c_write(bus, _reverse_lsb_msb_16(reg), 16, &data, 1);
}

int vl53l1x_write_reg_word(uint16_t reg, uint16_t data)
{
	uint8_t buf[2];
	buf[0] = data >> 8;
	buf[1] = data & 0x00FF;
	return voxl_i2c_write(bus, _reverse_lsb_msb_16(reg), 16, buf, 2);
}

int vl53l1x_write_reg_int(uint16_t reg, uint32_t data)
{
	uint8_t buf[4];
	buf[0] = (data >> 24) & 0xFF;
	buf[1] = (data >> 16) & 0xFF;
	buf[2] = (data >> 8)  & 0xFF;
	buf[3] = (data >> 0)  & 0xFF;
	return voxl_i2c_write(bus, _reverse_lsb_msb_16(reg), 16, buf, 4);
}

int vl53l1x_read_reg_byte(uint16_t reg, uint8_t* data)
{
	int ret;
	ret = voxl_i2c_read(bus, _reverse_lsb_msb_16(reg), 16, data, 1);
	return ret;
}


int vl53l1x_read_reg_word(uint16_t reg, uint16_t* data)
{
	int ret;
	uint8_t buf[2];
	ret = voxl_i2c_read(bus, _reverse_lsb_msb_16(reg), 16, buf, 2);
	if(ret) return -1;
	*data = (buf[0] << 8) + buf[1];
	return 0;
}


int vl53l1x_start_ranging()
{
	return vl53l1x_write_reg_byte(SYSTEM__MODE_START, 0x40); /* Enable VL53L1X */
}

int vl53l1x_stop_ranging()
{
	return vl53l1x_write_reg_byte(SYSTEM__MODE_START, 0x00); /* Disable VL53L1X */
}

int vl53l1x_clear_interrupt()
{
	return vl53l1x_write_reg_byte(SYSTEM__INTERRUPT_CLEAR, 0x01);
}

int vl53l1x_check_for_data_ready(uint8_t *isDataReady)
{
	uint8_t Temp;

	if(vl53l1x_read_reg_byte(GPIO__TIO_HV_STATUS, &Temp)){
		return -1;
	}

	if(Temp & 1){
		*isDataReady = 1;
	}
	else{
		*isDataReady = 0;
	}
	return 0;
}

int vl53l1x_get_distance_mm(int* dist_mm)
{
	// first check status, if it's not valid, don't bother reading the distance
	uint8_t status;
	if(vl53l1x_read_reg_byte(VL53L1_RESULT__RANGE_STATUS, &status)){
		return -1;
	}
	status = status & 0x1F;
	if(status!=9){
		*dist_mm = -1000;
		return 0;
	}

	uint16_t tmp;
	if(vl53l1x_read_reg_word(VL53L1_RESULT__FINAL_CROSSTALK_CORRECTED_RANGE_MM_SD0, &tmp)){
		return -1;
	}
	*dist_mm = tmp;
	return 0;
}

int vl53l1x_set_address(uint8_t addr)
{
	return vl53l1x_write_reg_byte(VL53L1_I2C_SLAVE__DEVICE_ADDRESS, addr); /* Enable VL53L1X */
}

static int vl53l1x_check_whoami(int quiet)
{
	//read WHOAMI register
	uint16_t id;
	int ret = vl53l1x_read_reg_word(VL53L1_IDENTIFICATION__MODEL_ID, &id);
	if(ret<0){
		if(!quiet){
			fprintf(stderr, "ERROR in %s, failed to read whoami register\n", __FUNCTION__);
		}
		return -2;
	}
	if(id != 0xEACC){
		fprintf(stderr, "ERROR in %s, invalid whoami register\n", __FUNCTION__);
		fprintf(stderr, "read %X, expected 0xEACC\n", id);
		return -1;
	}
	return 0;
}


static int vl53l1x_swap_to_secondary_address()
{
	// turn off mux so we are only talking to the device not on mux
	if(_set_slave(MUX_NONE, VL53L1X_TOF_DEFAULT_ADDRESS)){
		fprintf(stderr, "ERROR in %s, couldn't set slave\n", __FUNCTION__);
		return -1;
	}

	// check whoami at default
	if(vl53l1x_check_whoami(1)==0){
		// device is at default address, put it to secondary
		printf("swapping to secondary\n");
		vl53l1x_set_address(nonmux_sensor_address);
		_set_slave(MUX_NONE, nonmux_sensor_address);
		usleep(1000);
		// now swap back and check if it worked
		if(vl53l1x_check_whoami(1)==0){
			printf("successfully swapped to secondary\n");
			return 0;
		}
		else{
			fprintf(stderr, "something went wrong\n");
			return -1;
		}
	}
	else{
		printf("checking if secondary is set already\n");
		_set_slave(MUX_NONE, nonmux_sensor_address);
		if(vl53l1x_check_whoami(1)==0){
			printf("device already on secondary\n");
			return 0;
		}
		else{
			fprintf(stderr, "ERROR in %s, can't talk to vl53l1X on either primary or secondary address\n", __FUNCTION__);
			return -1;
		}
	}
	return 0;
}


// argument is the index of this sensor in the enabled_sensors array
static int vl53l1x_init(int sensor)
{
	if(vl53l1x_check_whoami(0)){
		fprintf(stderr, "ERROR in %s, failed to verify whoami\n", __FUNCTION__);
		return -1;
	}

	// load in default settings
	uint8_t Addr = 0x00;
	for (Addr = 0x2D; Addr <= 0x87; Addr++){
		vl53l1x_write_reg_byte(Addr, VL51L1X_DEFAULT_CONFIGURATION[Addr - 0x2D]);
	}

	// set to long distance mode
	vl53l1x_write_reg_byte(PHASECAL_CONFIG__TIMEOUT_MACROP, 0x0A);
	vl53l1x_write_reg_byte(RANGE_CONFIG__VCSEL_PERIOD_A, 0x0F);
	vl53l1x_write_reg_byte(RANGE_CONFIG__VCSEL_PERIOD_B, 0x0D);
	vl53l1x_write_reg_byte(RANGE_CONFIG__VALID_PHASE_HIGH, 0xB8);
	vl53l1x_write_reg_word(SD_CONFIG__WOI_SD0, 0x0F0D);
	vl53l1x_write_reg_word(SD_CONFIG__INITIAL_PHASE_SD0, 0x0E0E);

	// set timing budget to 50ms
	int TimingBudgetInMs = 50;
	switch (TimingBudgetInMs)
	{
		case 20:
			vl53l1x_write_reg_word(RANGE_CONFIG__TIMEOUT_MACROP_A_HI,0x001E);
			vl53l1x_write_reg_word(RANGE_CONFIG__TIMEOUT_MACROP_B_HI,0x0022);
			break;
		case 33:
			vl53l1x_write_reg_word(RANGE_CONFIG__TIMEOUT_MACROP_A_HI,0x0060);
			vl53l1x_write_reg_word(RANGE_CONFIG__TIMEOUT_MACROP_B_HI,0x006E);
			break;
		case 50:
			vl53l1x_write_reg_word(RANGE_CONFIG__TIMEOUT_MACROP_A_HI,0x00AD);
			vl53l1x_write_reg_word(RANGE_CONFIG__TIMEOUT_MACROP_B_HI,0x00C6);
			break;
		case 100:
			vl53l1x_write_reg_word(RANGE_CONFIG__TIMEOUT_MACROP_A_HI,0x01CC);
			vl53l1x_write_reg_word(RANGE_CONFIG__TIMEOUT_MACROP_B_HI,0x01EA);
			break;
		case 200:
			vl53l1x_write_reg_word(RANGE_CONFIG__TIMEOUT_MACROP_A_HI,0x02D9);
			vl53l1x_write_reg_word(RANGE_CONFIG__TIMEOUT_MACROP_B_HI,0x02F8);
			break;
		case 500:
			vl53l1x_write_reg_word(RANGE_CONFIG__TIMEOUT_MACROP_A_HI,0x048F);
			vl53l1x_write_reg_word(RANGE_CONFIG__TIMEOUT_MACROP_B_HI,0x04A4);
			break;
		default:
			fprintf(stderr, "invalid timing budget\n");
			return -1;
	}

	// set optical center to the middle
	vl53l1x_write_reg_byte(ROI_CONFIG__USER_ROI_CENTRE_SPAD, 199);

	// pick correct SPAD size between 4x4 to 16x16 for desired fov
	// also set the FOV that will actually be set in the enabled_sensors struct
	// so that this number will be sent out the pipe instead of what was
	// in the config file
	uint8_t pads = 4;
	if(enabled_sensors[sensor].fov_deg >= 26.125){
		enabled_sensors[sensor].fov_deg = 27.0;
		pads = 16;
	}
	else if(enabled_sensors[sensor].fov_deg >= 24.375){
		enabled_sensors[sensor].fov_deg = 25.25;
		pads = 14;
	}
	else if(enabled_sensors[sensor].fov_deg >= 22.625){
		enabled_sensors[sensor].fov_deg = 23.5;
		pads = 12;
	}
	else if(enabled_sensors[sensor].fov_deg >= 20.875){
		enabled_sensors[sensor].fov_deg = 21.75;
		pads = 10;
	}
	else if(enabled_sensors[sensor].fov_deg >= 18.75){
		enabled_sensors[sensor].fov_deg = 20.0;
		pads = 8;
	}
	else if(enabled_sensors[sensor].fov_deg >= 16.25){
		enabled_sensors[sensor].fov_deg = 17.5;
		pads = 6;
	}
	else{
		enabled_sensors[sensor].fov_deg = 15.0;
		pads = 4;
	}

	if(en_debug){
		printf("using %2d pads, for a diagonal fov of %6.1f deg\n", pads, enabled_sensors[sensor].fov_deg);
	}

	vl53l1x_write_reg_byte(ROI_CONFIG__USER_ROI_REQUESTED_GLOBAL_XY_SIZE,  (pads-1)<<4 | (pads-1));


	// stuff for automatic intermeasurement period, not used here
	// uint16_t ClockPLL;
	// uint16_t intermeasurement_time_ms = 30;
	// vl53l1x_read_reg_word(VL53L1_RESULT__OSC_CALIBRATE_VAL, &ClockPLL);
	// ClockPLL = ClockPLL & 0x3FF;
	// vl53l1x_write_reg_int(VL53L1_SYSTEM__INTERMEASUREMENT_PERIOD,
	// 			   (uint32_t)(ClockPLL * intermeasurement_time_ms * 1.075));

	return 0;
}


static int vl53l1x_wait_for_data()
{
	for(int i=0; i<100; i++){
		uint8_t isDataReady = 0;
		if(vl53l1x_check_for_data_ready(&isDataReady)){
			fprintf(stderr, "failed to check data ready\n");
			return -1;
		}
		if(isDataReady){
			return 0;
		}
		//printf("data ready: %d i=%d\n", isDataReady, i);
		usleep(1000);
	}

	// timeout
	return -1;
}






static int sx1509_read_reg_word(uint8_t reg, uint16_t* data)
{
	int ret;
	uint8_t buf[2];
	ret = voxl_i2c_read(bus, reg, 8, buf, 2);
	if(ret) return -1;
	*data = (buf[0] << 8) + buf[1];
	return 0;
}


static int sx1509_write_reg_byte(uint8_t reg, uint8_t data)
{
	return voxl_i2c_write(bus, reg, 8, &data, 1);
}

static int sx1509_write_reg_word(uint8_t reg, uint16_t data)
{
	uint8_t buf[2];
	buf[0] = data >> 8;
	buf[1] = data & 0x00FF;
	return voxl_i2c_write(bus, reg, 8, buf, 2);
}




static int sx1509_init(uint8_t address)
{
	_set_slave(MUX_NONE, address);

	// SX1509 doens't have a whoami, so read in RegInterruptMaskA and RegSenseHighB
	// in one read, this should return 0xFF00 when the chip starts up
	uint16_t testRegisters = 0;
	int ret = sx1509_read_reg_word(REG_INTERRUPT_MASK_A, &testRegisters);

	if(ret<0){
		fprintf(stderr, "ERROR in %s, failed to read whoami\n", __FUNCTION__);
		return -1;
	}

	if(testRegisters != 0xFF00){
		fprintf(stderr, "ERROR in %s, failed to verify whoami\n", __FUNCTION__);
		fprintf(stderr, "got %X, expected 0xFF00\n", testRegisters);
		return -1;
	}

	// Set the clock
	sx1509_write_reg_word(REG_CLOCK, 0x4F);
	sx1509_write_reg_word(REG_MISC, 0x70);

	// disable input for only the pins we are using
	sx1509_write_reg_word(REG_INPUT_DISABLE_B, gpio_pin_output_mask);
	// set pins we are going to use as output. 0 is output, 1 is input
	sx1509_write_reg_word(REG_DIR_B, ~gpio_pin_output_mask);

	// set all output pins low for now
	if(sx1509_write_reg_word(REG_DATA_B, 0x0000)){
		fprintf(stderr, "ERROR in %s, failed to write register\n", __FUNCTION__);
		return -1;
	}

	return 0;
}


static int sx1509_gpio_set_pin(int pin, int val)
{
	uint16_t data;
	if(sx1509_read_reg_word(REG_DATA_B, &data)){
		fprintf(stderr, "ERROR in %s, failed to read data register\n", __FUNCTION__);
		return -1;
	}

	// turn on/off the
	if(val)	data |= (1<<pin);
	else	data &= ~(1<<pin);

	if(sx1509_write_reg_word(REG_DATA_B, data)){
		fprintf(stderr, "ERROR in %s, failed to write register\n", __FUNCTION__);
		return -1;
	}
	return 0;
}

static int sx1509_gpio_set_all_pins(uint16_t vals)
{
	if(sx1509_write_reg_word(REG_DATA_B, vals)){
		fprintf(stderr, "ERROR in %s, failed to write register\n", __FUNCTION__);
		return -1;
	}
	return 0;
}


static int sx1509_turn_on_all_enabled()
{
	if(sx1509_write_reg_word(REG_DATA_B, gpio_pin_enable_mask)){
		fprintf(stderr, "ERROR in %s, failed to write register\n", __FUNCTION__);
		return -1;
	}

	return 0;
}




static void _quit(int ret)
{
	if(dist_mm != NULL){
		printf("freeing RPC shared memory\n");
		voxl_rpc_shared_mem_free((uint8_t*)dist_mm);
		voxl_rpc_shared_mem_deinit();
	}

	printf("closing i2c bus\n");
	if(voxl_i2c_close(bus)){
		fprintf(stderr, "failed to close bus\n");
	}
	printf("closing pipes\n");
	pipe_server_close_all();
	printf("exiting\n");
	exit(ret);
	return;
}


int main(int argc, char* argv[])
{
	int i;

	// check for options
	if(__parse_opts(argc, argv)) return -1;

	// write out a new config file and quit if requested
	if(en_config_mode){
		return _write_new_config_file_with_defaults(config_arrangement);
	}

	// read in config file
	if(_read_config_file()){
		return -1;
	}

	if(en_debug){
		_print_config();
	}

////////////////////////////////////////////////////////////////////////////////
// gracefully handle an existing instance of the process and associated PID file
////////////////////////////////////////////////////////////////////////////////

	// make sure another instance isn't running
	// if return value is -3 then a background process is running with
	// higher privaledges and we couldn't kill it, in which case we should
	// not continue or there may be hardware conflicts. If it returned -4
	// then there was an invalid argument that needs to be fixed.
	if(kill_existing_process(SERVER_NAME, 2.0)<-2) return -1;

	// start signal handler so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		return -1;
	}

	printf("allocating RPC shared memory\n");
	dist_mm = (int*)voxl_rpc_shared_mem_alloc(MAX_SENSORS*sizeof(int));
	if(dist_mm==NULL){
		fprintf(stderr, "failed to allocate RPC shared memory\n");
		return -1;
	}


	printf("initializing i2c bus\n");
	if(voxl_i2c_init(bus)){
		fprintf(stderr, "failed to init bus\n");
		return -1;
	}

	// if we have an sx1509 gpio expander, start it up
	if(has_gpio_expander){
		printf("initializing sx1509 gpio expander at address %X\n", gpio_expander_address);
		if(sx1509_init(gpio_expander_address)){
			_quit(-1);
		}
		if(gpio_expander_port_for_nomux_sensor>=0){
			printf("turning on gpio pins for enabled sensors\n");
			if(sx1509_turn_on_all_enabled()){
				_quit(-1);
			}
		}
	}
	else{
		printf("skipping sx1509 gpio expander init\n");
	}

	// let sensors wake up, todo check if this is needed
	usleep(10000);

	// init all sensors, starting with the non-mux one
	if(has_nonmux_sensor){
		printf("swapping nomux tof sensor to secondary address %X\n", nonmux_sensor_address);
		if(vl53l1x_swap_to_secondary_address()){
			fprintf(stderr, "failed to set standalone sensor to secondary address\n");
			_quit(-1);
		}
		printf("initializing nomux tof sensor\n");
		if(vl53l1x_init(nonmux_sensor_index)){
			_quit(-1);
		}
	}
	// now set all the multiplexed ports
	for(i=0;i<n_enabled_sensors;i++){

		// skip the nomux sensor on expander
		if(enabled_sensors[i].is_on_mux == 0){
			printf("skipping nomux sensor, already done\n");
			continue;
		}

		printf("initializing multiplexed tof sensor id %d at mux port %d\n", enabled_sensors[i].sensor_id, enabled_sensors[i].i2c_mux_port);

		if(_set_slave(enabled_sensors[i].i2c_mux_port, VL53L1X_TOF_DEFAULT_ADDRESS)){
			fprintf(stderr, "failed to set slave\n");
			_quit(-1);
		}

		if(vl53l1x_init(i)){
			_quit(-1);
		}
	}


	// create the pipe
	pipe_info_t info = { \
		.name        = RANGEFINDER_PIPE_NAME,\
		.location    = RANGEFINDER_PIPE_LOCATION ,\
		.type        = "rangefinder_data_t",\
		.server_name = SERVER_NAME,\
		.size_bytes  = RANGEFINDER_RECOMMENDED_PIPE_SIZE};

	if(pipe_server_create(PIPE_CH, info, 0)){
		_quit(-1);
	}

	// pre-fill an array of data structs to send out the pipe
	rangefinder_data_t data[MAX_SENSORS];
	for(i=0; i<n_enabled_sensors;i++){
		data[i].magic_number			= RANGEFINDER_MAGIC_NUMBER;
		data[i].timestamp_ns			= 0;
		data[i].sample_id				= 0;
		data[i].sensor_id				= enabled_sensors[i].sensor_id;
		data[i].distance_m				= 0.0f;
		data[i].uncertainty_m			= enabled_sensors[i].uncertainty_m;
		data[i].fov_deg					= enabled_sensors[i].fov_deg;
		data[i].location_wrt_body[0]	= enabled_sensors[i].location_wrt_body[0];
		data[i].location_wrt_body[1]	= enabled_sensors[i].location_wrt_body[1];
		data[i].location_wrt_body[2]	= enabled_sensors[i].location_wrt_body[2];
		data[i].direction_wrt_body[0]	= enabled_sensors[i].direction_wrt_body[0];
		data[i].direction_wrt_body[1]	= enabled_sensors[i].direction_wrt_body[1];
		data[i].direction_wrt_body[2]	= enabled_sensors[i].direction_wrt_body[2];
		data[i].range_max_m				= enabled_sensors[i].range_max_m;
		data[i].type					= enabled_sensors[i].type;
		data[i].reserved				= 0;
	}




#ifdef SDSP_ACCELERATION
	// send data to sdsp defining how it should read the sensors
	rangefinder_rpc_sensor_data_for_sdsp_t sensor_data;
	sensor_data.bus = bus;
	sensor_data.n_enabled_sensors = n_enabled_sensors;
	sensor_data.has_nonmux_sensor = has_nonmux_sensor;
	sensor_data.n_mux_sensors = n_mux_sensors;
	sensor_data.nonmux_sensor_address = nonmux_sensor_address;
	sensor_data.nonmux_sensor_index = nonmux_sensor_index;
	sensor_data.mux_address = mux_address;
	sensor_data.mux_sensor_address = VL53L1X_TOF_DEFAULT_ADDRESS;
	sensor_data.wait_time_us = 45000;
	for(i=0;i<n_enabled_sensors;i++){
		sensor_data.mux_port[i] = enabled_sensors[i].i2c_mux_port;
	}
	if(!has_nonmux_sensor){
		sensor_data.nonmux_sensor_index = -1;
	}
	if(rangefinder_rpc_set_sensor_data(&sensor_data)){
		fprintf(stderr, "ERROR, failed to send sensor data to SDSP\n");
		_quit(-1);
	}
#endif // SDSP_ACCELERATION



	// keep track of timing
	int64_t last_time_ns = _apps_time_monotonic_ns();

	// keep sampling until signal handler tells us to stop
	main_running = 1;
	while(main_running){

		// nothing to do if there are no clients and not in debug mode
		if(pipe_server_get_num_clients(PIPE_CH)==0 && !en_debug){
			usleep(500000);
			continue;
		}


#ifndef SDSP_ACCELERATION

		// small array to keep the distances in
		int dist_mm[MAX_SENSORS];

		// start the standalone sensor ranging if it exists
		if(has_nonmux_sensor){
			_set_slave(MUX_NONE, nonmux_sensor_address);
			if(vl53l1x_start_ranging()){
				fprintf(stderr, "failed to start ranging\n");
				main_running = 0;
				break;
			}
		}
		// start all the multiplexed sensors reading at the same time
		if(n_mux_sensors>0){
			_set_slave(MUX_ALL, VL53L1X_TOF_DEFAULT_ADDRESS);
			if(vl53l1x_start_ranging()){
				fprintf(stderr, "failed to start ranging\n");
				main_running = 0;
				break;
			}
		}

		// sleep a bit while they range, this should be less than the actual
		// ranging time so we can poll them at the end of the ranging process
		usleep(33000);

		// now start reading the data back in
		for(i=0;i<n_enabled_sensors;i++){

			// switch i2c bus and multiplexer over to either a multiplexed or non-multiplexed sensor
			if(enabled_sensors[i].is_on_mux){
				_set_slave(enabled_sensors[i].i2c_mux_port, VL53L1X_TOF_DEFAULT_ADDRESS);
			}
			else{
				_set_slave(MUX_NONE, nonmux_sensor_address);
			}

			// wait for it to be done ranging
			if(vl53l1x_wait_for_data()){
				main_running = 0;
				break;
			}

			// read in the data and stop it ranging
			vl53l1x_get_distance_mm(&dist_mm[i]);
			vl53l1x_clear_interrupt();
			vl53l1x_stop_ranging();
		}

#else
		// let the sdsp read all the rangefinders one and return the result
		if(rangefinder_rpc_read_all_rangefinders((uint8_t*)dist_mm, MAX_SENSORS*sizeof(int))){
			fprintf(stderr, "ERROR, failed to read sensor data from sdsp\n");
			_quit(-1);
		}

#endif // SDSP_ACCELERATION


		// grab time at the end of ranging
		// assume timestamp of data was from halfway through the reading process
		// TODO validate this experimentally
		int64_t time_ns = _apps_time_monotonic_ns();
		int64_t timestamp_ns = time_ns - ((time_ns-last_time_ns)/2);

		// keep track of number of samples read
		static int sample_id = 0;
		sample_id ++;

		// populate data for pipe and send it out all at once
		for(i=0; i<n_enabled_sensors;i++){
			data[i].timestamp_ns			= timestamp_ns;
			data[i].sample_id				= sample_id;
			data[i].distance_m				= dist_mm[i]/1000.0;;
		}
		pipe_server_write(PIPE_CH, data, sizeof(rangefinder_data_t)*n_enabled_sensors);

		// print distances in debug mode
		if(en_debug){
			double dt_ms = (time_ns-last_time_ns)/1000000.0;
			printf("dt = %6.1fms ", dt_ms);

			printf("dist_mm: ");
			for(i=0;i<n_enabled_sensors;i++){
				printf(" %6d", dist_mm[i]);
			}
			printf("\n");
		}

		last_time_ns = time_ns;
	}

	// close and cleanup
	printf("exiting cleanly\n");
	_quit(0);
	return 0;
}
