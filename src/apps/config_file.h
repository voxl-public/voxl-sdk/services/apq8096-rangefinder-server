/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#include <stdio.h>
#include <string.h>
#include <modal_json.h>

#include <voxl_rangefinder_interface.h>
#include <common.h>

#define CONFIG_FILE_PATH	"/etc/modalai/voxl-rangefinder-server.conf"
#define SIN45 0.707106781186547524400844362105

// struct to contain all data from each single tag entry in config file
typedef struct rangefinder_config_t{

	int enabled;					///< a sensor is allowed to be listed and configured but disabled
	int sensor_id;					///< ID of the rangefinder, must be unique
	int type;						///< see voxl_rangefinder_interface.h
	int i2c_address;				// for VL53l1X this is the desired address that will be set


	float uncertainty_m;			///< encertainty in meters. Set negative if unknown
	float fov_deg;					///< field of view of the sensor in degrees
	float range_max_m;				///< Maximum range of the sensor in meters

	float location_wrt_body[3];		///< location of the rangefinder with respect to body frame.
	float direction_wrt_body[3];	///< direction vector of the rangefinder with respect to body frame

	int is_on_mux;					// set non-zero to indicate this is connected through an i2c multiplexer
	int i2c_mux_address;			// multiplexer address
	int i2c_mux_port;				// 1-8

	// use of gpio pins for VL53L1X is optional. port1 may be connected to the XSHUT
	// pin on the rangefinder to shut it down while programming other devices
	// that are not on a multiplexer. port2 is unused always.
	//
	// for CH201 TDK Chirp sensors. port 1 is connected to the PROG pin, port 2 on the INT pin
	int is_on_gpio_expander;
	int gpio_expander_address;
	int gpio_expander_port1;
	int gpio_expander_port2;

} rangefinder_config_t;



// all sensors, including disabled ones
// everything else is just concerned with the enabled_sensors array
static int n_total_sensors;
static rangefinder_config_t r[MAX_SENSORS];


// all enabled sensors and some easy-access data about them
static int n_enabled_sensors;
static rangefinder_config_t enabled_sensors[MAX_SENSORS];
static int has_nonmux_sensor = 0;
static int nonmux_sensor_index = 0;
static int nonmux_sensor_address = 0;
static int has_gpio_expander = 0;
static int gpio_expander_address = 0;
static int gpio_expander_port_for_nomux_sensor = -1;
static uint16_t gpio_pin_output_mask = 0; // bitmask of pinsused as outputs on gpio expander
static uint16_t gpio_pin_enable_mask = 0;
static int n_mux_sensors = 0;
static int mux_address = 0;
static int bus;



#define CONFIG_FILE_HEADER "\
/**\n\
 * Rangefinder Configuration File\n\
 * This file is used by voxl-rangefinder-server\n\
 * please use voxl-rangefinder-server --config {arrangement}\n\
 * to set up this file.\n\
 *\n\
 * FOV for VL53l1X TOF rangefinder is a diagonal FOV in degrees and\n\
 * can be set between 15 and 27 degrees.\n\
 */\n"


// return a default config struct. used in other functions to save space and remain
// consistent as to what the defaults are.
static rangefinder_config_t _get_default_config()
{
	rangefinder_config_t r;

	r.enabled = 1;
	r.sensor_id = 0;
	r.type = RANGEFINDER_TYPE_TOF_VL53L1X;
	r.i2c_address = VL53L1X_TOF_DEFAULT_ADDRESS;
	r.uncertainty_m = 0.02;
	r.fov_deg = 27;
	r.range_max_m = 3.0;
	r.location_wrt_body[0] = 0.0;
	r.location_wrt_body[1] = 0.0;
	r.location_wrt_body[2] = 0.0;
	r.direction_wrt_body[0] = 0.0;
	r.direction_wrt_body[1] = 0.0;
	r.direction_wrt_body[2] = 0.0;
	r.is_on_mux = 1;
	r.i2c_mux_address = TCA9548A_MUX_DEFAULT_ADDR_BREADBOARD;
	r.i2c_mux_port = 0;
	r.is_on_gpio_expander = 0;
	r.gpio_expander_address = SX1509B_GPIO_DEFAULT_ADDRESS;
	r.gpio_expander_port1 = 0;
	r.gpio_expander_port2 = 1;

	return r;
}


static void _print_config()
{
	int i,j;
	const char* type_strings[] = RANGEFINDER_TYPE_STRINGS;

	printf("i2c_bus: %d\n", bus);

	for(i=0; i<n_total_sensors; i++){
		printf("#%d:\n",i);
		printf("    enabled:               %d\n", r[i].enabled);
		printf("    sensor_id:             %d\n", r[i].sensor_id);
		printf("    type:                  %s\n", type_strings[r[i].type]);
		printf("    i2c_address:           0x%X\n", r[i].i2c_address);

		printf("    uncertainty_m:         %0.3f\n", (double)r[i].uncertainty_m);
		printf("    fov_deg:               %0.3f\n", (double)r[i].fov_deg);
		printf("    range_max_m:           %0.3f\n", (double)r[i].range_max_m);

		printf("    location_wrt_body:     ");
		for(j=0;j<3;j++) printf("%0.1f ",r[i].location_wrt_body[j]);
		printf("\n");
		printf("    direction_wrt_body:    ");
		for(j=0;j<3;j++) printf("%0.1f ",r[i].direction_wrt_body[j]);
		printf("\n");

		printf("    is_on_mux:             %d\n", r[i].is_on_mux);
		printf("    i2c_mux_address:       0x%X\n", r[i].i2c_mux_address);
		printf("    i2c_mux_port:          %d\n", r[i].i2c_mux_port);

		printf("    is_on_gpio_expander:   %d\n", r[i].is_on_gpio_expander);
		printf("    gpio_expander_address: 0x%X\n", r[i].gpio_expander_address);
		printf("    gpio_expander_port_1:  %d\n", r[i].gpio_expander_port1);
		printf("    gpio_expander_port_2:  %d\n", r[i].gpio_expander_port2);

		printf("\n");
	}
	return;
}



static int _read_config_file()
{
	// vars and defaults
	int i;
	const char* type_strings[] = RANGEFINDER_TYPE_STRINGS;
	rangefinder_config_t default_r = _get_default_config();

	// set number of sensors to 0 at first in case there is an error
	n_total_sensors = 0;
	n_enabled_sensors = 0;

	// check file exists
	if(access(CONFIG_FILE_PATH, F_OK) == -1){
		printf("no config file found, please run voxl-configure-rangefinders\n");
		return -1;
	}

	// read the data in
	cJSON* parent = json_read_file(CONFIG_FILE_PATH);
	if(parent==NULL){
		printf("error reading config file, please run voxl-configure-rangefinders\n");
		return -1;
	}

	// file structure is just one big array of rangefinder_config_t structures
	cJSON* json_array = json_fetch_array_and_add_if_missing(parent, "sensors", &n_total_sensors);
	if(n_total_sensors > MAX_SENSORS){
		fprintf(stderr, "ERROR found %d sensors in file but maximum number is %d\n", n_total_sensors, MAX_SENSORS);
		return -1;
	}

	// no sensors found
	if(n_total_sensors == 0){
		printf("no sensors found in config file, please run voxl-configure-rangefinders\n");
		return -1;
	}

	// for now, the i2c bus is the only thing not in the array
	// ports from VOXL1 here for reference
	// #define I2C_J11	12	// BLSP 12  on physical port J11 pins 4 (sda) and 6 (SCL)
	// #define I2C_J10	7	// BLSP 7   on physical port J10 pins 4 (sda) and 6 (SCL)
	json_fetch_int_with_default(parent, "i2c_bus", &bus, 7);

	// copy out each item in the array
	for(i=0; i<n_total_sensors; i++){
		cJSON* json_item = cJSON_GetArrayItem(json_array, i);

		json_fetch_bool_with_default(json_item, "enabled", &r[i].enabled, default_r.enabled);
		json_fetch_int_with_default(json_item, "sensor_id", &r[i].sensor_id, i);
		json_fetch_enum_with_default(json_item, "type",	&r[i].type, type_strings, N_RANGEFINDER_TYPES, default_r.type);
		json_fetch_int_with_default(json_item, "i2c_address", &r[i].i2c_address, default_r.i2c_address);

		json_fetch_float_with_default(json_item, "uncertainty_m", &r[i].uncertainty_m, default_r.uncertainty_m);
		json_fetch_float_with_default(json_item, "fov_deg", &r[i].fov_deg, default_r.fov_deg);
		json_fetch_float_with_default(json_item, "range_max_m", &r[i].range_max_m, default_r.range_max_m);

		json_fetch_fixed_vector_float_with_default(json_item, "location_wrt_body",	r[i].location_wrt_body, 3,	default_r.location_wrt_body);
		json_fetch_fixed_vector_float_with_default(json_item, "direction_wrt_body",	r[i].direction_wrt_body, 3,	default_r.direction_wrt_body);

		json_fetch_bool_with_default(json_item, "is_on_mux", &r[i].is_on_mux, default_r.is_on_mux);
		json_fetch_int_with_default(json_item, "i2c_mux_address", &r[i].i2c_mux_address, default_r.i2c_mux_address);
		json_fetch_int_with_default(json_item, "i2c_mux_port", &r[i].i2c_mux_port, default_r.i2c_mux_port);

		json_fetch_bool_with_default(json_item, "is_on_gpio_expander", &r[i].is_on_gpio_expander, default_r.is_on_gpio_expander);
		json_fetch_int_with_default(json_item, "gpio_expander_address", &r[i].gpio_expander_address, default_r.gpio_expander_address);
		json_fetch_int_with_default(json_item, "gpio_expander_port1", &r[i].gpio_expander_port1, default_r.gpio_expander_port1);
		json_fetch_int_with_default(json_item, "gpio_expander_port2", &r[i].gpio_expander_port2, default_r.gpio_expander_port2);
	}

	// check if we got any errors in that process
	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse data in %s\n", CONFIG_FILE_PATH);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		printf("The JSON data was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(CONFIG_FILE_PATH, parent, CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);


	// now go through the sensors to figure out the higher level information
	for(i=0; i<n_total_sensors; i++){

		// keep an array of just the enabled sensors to read from later
		if(r[i].enabled){
			n_enabled_sensors++;
			enabled_sensors[n_enabled_sensors-1] = r[i];
		}

		// make sure mux port is in 0-7
		if(r[i].is_on_mux==1 && r[i].enabled){
			if(r[i].i2c_mux_port<0 || r[i].i2c_mux_port>7){
				fprintf(stderr, "ERROR reading config file, i2c_mux_port must be in 0-7\n");
				return -1;
			}
			n_mux_sensors++;
			mux_address = r[i].i2c_mux_address;
		}

		// flag if there is an odd sensor not on a multiplexer
		if(r[i].is_on_mux==0 && r[i].enabled){

			// only allow one not on a multiplexer
			if(has_nonmux_sensor){
				fprintf(stderr, "ERROR parsing config file, can only have one sensor not on multiplexer right now\n");
				return -1;
			}
			// the nonmux sensor must be on a different address
			if(r[i].i2c_address == VL53L1X_TOF_DEFAULT_ADDRESS){
				fprintf(stderr, "ERROR parsing config file, can only have one sensor not on multiplexer right now\n");
				return -1;
			}

			has_nonmux_sensor = 1;
			nonmux_sensor_address = r[i].i2c_address;
			nonmux_sensor_index = n_enabled_sensors - 1;
		}

		// todo check for multiple expanders
		if(r[i].is_on_gpio_expander){
			has_gpio_expander = 1;
			gpio_expander_address = r[i].gpio_expander_address;
			gpio_pin_output_mask |= 1 << r[i].gpio_expander_port1;

			if(r[i].enabled){
				gpio_expander_port_for_nomux_sensor = r[i].gpio_expander_port1;
				gpio_pin_enable_mask |= 1 << r[i].gpio_expander_port1;
			}
		}
	}

	if(n_mux_sensors>8){
		fprintf(stderr, "ERROR parsing config file, can only have up to 8 sensors on i2c multiplexer right now\n");
		return -1;
	}

	return 0;
}










// used when constructing default sensor configurations
static int _add_rangefinder_config_to_json(rangefinder_config_t* r, int n, int bus, cJSON* parent)
{
	int i,m;
	const char* type_strings[] = RANGEFINDER_TYPE_STRINGS;

	cJSON_AddNumberToObject(parent, "i2c_bus", bus);
	cJSON* json_array = json_fetch_array_and_add_if_missing(parent, "sensors", &m);

	for(i=0;i<n;i++){
		cJSON* json_item = cJSON_CreateObject();
		cJSON_AddItemToArray(json_array, json_item);

		cJSON_AddBoolToObject(json_item, "enabled", r[i].enabled);
		cJSON_AddNumberToObject(json_item, "sensor_id", r[i].sensor_id);
		cJSON_AddStringToObject(json_item, "type", type_strings[r[i].type]);
		cJSON_AddNumberToObject(json_item, "i2c_address", r[i].i2c_address);

		cJSON_AddNumberToObject(json_item, "uncertainty_m", r[i].uncertainty_m);
		cJSON_AddNumberToObject(json_item, "fov_deg", r[i].fov_deg);
		cJSON_AddNumberToObject(json_item, "range_max_m", r[i].range_max_m);

		cJSON_AddItemToObject(json_item, "location_wrt_body", cJSON_CreateFloatArray(r[i].location_wrt_body, 3));
		cJSON_AddItemToObject(json_item, "direction_wrt_body", cJSON_CreateFloatArray(r[i].direction_wrt_body, 3));

		cJSON_AddBoolToObject(json_item, "is_on_mux", r[i].is_on_mux);
		cJSON_AddNumberToObject(json_item, "i2c_mux_address", r[i].i2c_mux_address);
		cJSON_AddNumberToObject(json_item, "i2c_mux_port", r[i].i2c_mux_port);

		cJSON_AddBoolToObject(json_item, "is_on_gpio_expander", r[i].is_on_gpio_expander);
		cJSON_AddNumberToObject(json_item, "gpio_expander_address", r[i].gpio_expander_address);
		cJSON_AddNumberToObject(json_item, "gpio_expander_port1", r[i].gpio_expander_port1);
		cJSON_AddNumberToObject(json_item, "gpio_expander_port2", r[i].gpio_expander_port2);
	}

	return 0;
}


#define RANGEFINDER_ARRANGEMENT_4_TOF_ON_BREADBOARD_MUX			1
#define RANGEFINDER_ARRANGEMENT_8_TOF_ON_BREADBOARD_MUX			2
#define RANGEFINDER_ARRANGEMENT_10_TOF_ON_VOXLCAM_SEEKER		3

static int _write_new_config_file_with_defaults(int arrangement)
{
	int bus,i;
	int n_sensors;
	rangefinder_config_t r[MAX_SENSORS];
	rangefinder_config_t default_r = _get_default_config();

	switch(arrangement){
		case RANGEFINDER_ARRANGEMENT_4_TOF_ON_BREADBOARD_MUX:

			printf("creating new config file for 4 TOF sensors on a sparkfun breadboard on port J10\n");

			bus = 7; // J10 on VOXL1
			n_sensors = 4;
			for(i=0;i<n_sensors;i++){
				r[i] = default_r;
				r[i].sensor_id = i;
				r[i].i2c_mux_port = i;
				r[i].is_on_gpio_expander = 0;
			}
			break;

		case RANGEFINDER_ARRANGEMENT_8_TOF_ON_BREADBOARD_MUX:

			printf("creating new config file for 8 TOF sensors on a sparkfun breadboard on port J10\n");

			bus = 7; // J10 on VOXL1
			n_sensors = 8;
			for(i=0;i<n_sensors;i++){
				r[i] = default_r;
				r[i].sensor_id = i;
				r[i].i2c_mux_port = i;
				r[i].is_on_gpio_expander = 0;
			}
			break;

		case RANGEFINDER_ARRANGEMENT_10_TOF_ON_VOXLCAM_SEEKER:

			printf("creating new config file for 10 TOF sensors on VOXLCAM/Seeker port J10\n");

			bus = 7; // J10 on VOXL1
			n_sensors = 10;
			// configure common values
			for(i=0;i<n_sensors;i++){
				r[i] = default_r;
				r[i].sensor_id = i;
				r[i].i2c_mux_address = TCA9548A_MUX_DEFAULT_ADDR_VOXLCAM;
				r[i].i2c_mux_port = i;
				r[i].is_on_gpio_expander = 1;
				r[i].gpio_expander_port1 = i;
			}

			// populate the last 2 forward-facing ones not on the mux
			r[8].enabled = 1;
			r[8].i2c_address = VL53L1X_TOF_DEFAULT_ADDRESS+1;
			r[8].is_on_mux = 0;

			// last one is disabled since it also faces forward
			r[9].enabled = 0;
			r[9].i2c_address = VL53L1X_TOF_DEFAULT_ADDRESS+2;
			r[9].is_on_mux = 0;


			// directions for all of them
			r[0].location_wrt_body[0]  =  0.060;
			r[0].location_wrt_body[1]  =  0.005;
			r[0].location_wrt_body[2]  = -0.016;
			r[0].direction_wrt_body[0] =  0.0;
			r[0].direction_wrt_body[1] =  0.0;
			r[0].direction_wrt_body[2] = -1.0;

			r[1].enabled = 0; // disabled since it interferes with the motor
			r[1].location_wrt_body[0]  =  0.060;
			r[1].location_wrt_body[1]  =  0.016;
			r[1].location_wrt_body[2]  = -0.005;
			r[1].direction_wrt_body[0] =  0.0;
			r[1].direction_wrt_body[1] =  1.0;
			r[1].direction_wrt_body[2] =  0.0;

			r[2].location_wrt_body[0]  =  0.060;
			r[2].location_wrt_body[1]  =  0.016;
			r[2].location_wrt_body[2]  =  0.005;
			r[2].direction_wrt_body[0] =  SIN45;
			r[2].direction_wrt_body[1] =  SIN45;
			r[2].direction_wrt_body[2] =  0.000;

			r[3].location_wrt_body[0]  =  0.060;
			r[3].location_wrt_body[1]  =  0.010;
			r[3].location_wrt_body[2]  =  0.016;
			r[3].direction_wrt_body[0] =  0.0;
			r[3].direction_wrt_body[1] =  0.0;
			r[3].direction_wrt_body[2] =  1.0;

			r[4].location_wrt_body[0]  =  0.060;
			r[4].location_wrt_body[1]  = -0.010;
			r[4].location_wrt_body[2]  =  0.016;
			r[4].direction_wrt_body[0] =  SIN45;
			r[4].direction_wrt_body[1] =  0.0;
			r[4].direction_wrt_body[2] =  SIN45;

			r[5].location_wrt_body[0]  =  0.060;
			r[5].location_wrt_body[1]  = -0.018;
			r[5].location_wrt_body[2]  =  0.005;
			r[5].direction_wrt_body[0] =  SIN45;
			r[5].direction_wrt_body[1] = -SIN45;
			r[5].direction_wrt_body[2] =  0.0;

			r[6].enabled = 0; // disabled since it interferes with the motor
			r[6].location_wrt_body[0]  =  0.060;
			r[6].location_wrt_body[1]  = -0.018;
			r[6].location_wrt_body[2]  =  0.005;
			r[6].direction_wrt_body[0] =  0.0;
			r[6].direction_wrt_body[1] = -1.0;
			r[6].direction_wrt_body[2] =  0.0;

			r[7].location_wrt_body[0]  =  0.060;
			r[7].location_wrt_body[1]  = -0.005;
			r[7].location_wrt_body[2]  = -0.016;
			r[7].direction_wrt_body[0] =  SIN45;
			r[7].direction_wrt_body[1] =  0.0;
			r[7].direction_wrt_body[2] = -SIN45;

			r[8].location_wrt_body[0]  = 0.06;
			r[8].location_wrt_body[1]  = -0.025;
			r[8].location_wrt_body[2]  = 0.0;
			r[8].direction_wrt_body[0] = 1.0;
			r[8].direction_wrt_body[1] = 0.0;
			r[8].direction_wrt_body[2] = 0.0;

			r[9].location_wrt_body[0] = 0.06;
			r[9].location_wrt_body[1] = 0.025;
			r[9].location_wrt_body[2] = 0.0;
			r[9].direction_wrt_body[0] = 1.0;
			r[9].direction_wrt_body[1] = 0.0;
			r[9].direction_wrt_body[2] = 0.0;
			break;

		default:
			fprintf(stderr, "ERROR in %s, invalid arrangement\n", __FUNCTION__);
			return -1;
	}

	cJSON* parent = cJSON_CreateObject();
	_add_rangefinder_config_to_json(r,n_sensors,bus, parent);
	json_write_to_file_with_header(CONFIG_FILE_PATH, parent, CONFIG_FILE_HEADER);
	cJSON_Delete(parent);

	printf("DONE\n");

	return 0;
}


#endif // end #define CONFIG_FILE_H
